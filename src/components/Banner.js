// import Button from 'react-bootstrap/Button'
// import Row from 'react-bootstrap/Row'
// import Col from 'react-bootstrap/Col'
import {Button, Row, Col} from 'react-bootstrap'
import {Link} from 'react-router-dom'



export default function Banner(props){

	return(
			(props.value===true) ?
			<Row>
				<Col className='p-5'>
					<h1>Zuitt Coding Bootcamp</h1>
					<p>Opportunities for everyone, everywhere</p>
					<Button variant='primary'>Enroll Now!</Button>
				</Col>
			</Row>
			:
			<Row>
				<Col className='p-5'>
					<h1>Page Not Found</h1>
					<p>Go to <Link to='/'>homepage</Link></p>
				</Col>
			</Row>
		)
}

