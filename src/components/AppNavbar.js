import Navbar from 'react-bootstrap/Navbar'
import Nav from 'react-bootstrap/Nav'
import {Link} from 'react-router-dom'
import {useState, Fragment,useContext} from 'react'
import UserContext from '../UserContext'

export default function AppNavbar(){

  // const [user,setUser]=useState(localStorage.getItem('email'))
  // console.log(user)
  const {user}=useContext(UserContext)

  return(
    <Navbar bg="light" expand="lg">
        <Navbar.Brand as={Link} to="/">Zuitt</Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="ml-auto">
            <Nav.Link as={Link} to="/">Home</Nav.Link>
            <Nav.Link as={Link} to="/courses">Courses</Nav.Link>
            {
              (user.id !== null) ?
              <Nav.Link as={Link} to="/logout">Logout</Nav.Link>
              :
              <Fragment>
                <Nav.Link as={Link} to="/register">Register</Nav.Link>
                <Nav.Link as={Link} to="/login">Login</Nav.Link>
              </Fragment>
            }
          </Nav>
        </Navbar.Collapse>
    </Navbar>
    )
}