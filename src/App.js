import {useState,useEffect} from 'react'
import {UserProvider} from './UserContext'
// import {Fragment} from 'react'
import {Container} from 'react-bootstrap'
import {BrowserRouter as Router} from 'react-router-dom'
import {Routes,Route} from 'react-router-dom'
import AppNavbar from './components/AppNavbar'
// import Banner from './components/Banner'
// import Highlights from './components/Highlights'
import Home from './pages/Home'
import Courses from './pages/Courses'
import Register from './pages/Register'
import Login from './pages/Login'
import Logout from './pages/Logout'
import Error from './pages/Error'
import CourseView from './pages/CourseView'
import './App.css'

function App() {

  const [user,setUser]=useState({
    // email:localStorage.getItem('email')
    id:null,
    isAdmin:null
  })

  // function to clear localstorage for logout
  const unsetUser=()=>{
    localStorage.clear()
  }

  useEffect(()=>{
    // console.log(user)
    // console.log(localStorage)
    fetch('http://localhost:4000/users/details',{
      method:'GET',
      headers:{
        Authorization:`Bearer ${localStorage.getItem('token')}`
      }
    })
    .then(res=>res.json())
    .then(data=>{
      if(typeof data._id!=='undefined'){
        setUser({
          id:data._id,
          isAdmin:data.isAdmin
        })
      } else {
        setUser({
          id:null,
          isAdmin:null
        })
      }
    })
  },[])

  return (
  <UserProvider value={{user,setUser,unsetUser}}>  
    <Router>
      <AppNavbar />
      <Container>
      <Routes>
        <Route exact path="/" element={<Home/>} />
        <Route exact path="/courses" element={<Courses/>} />
        <Route exact path="/login" element={<Login/>} />
        <Route exact path="/logout" element={<Logout/>} />
        <Route exact path="/register" element={<Register/>} />
        <Route exact path="*" element={<Error />} />
        <Route exact path='/courses/:courseId' element={<CourseView/>} />
      </Routes>
      </Container>
    </Router>
  </UserProvider>    
  );
}

export default App;
