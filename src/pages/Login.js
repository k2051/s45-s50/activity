import {Form,Button} from 'react-bootstrap'
import {Navigate} from 'react-router-dom'
import {useState,useEffect,useContext} from 'react'
import Swal from 'sweetalert2'
import UserContext from '../UserContext'

export default function Login(){

	const {user,setUser}=useContext(UserContext)
	const [email,setEmail]=useState('')
	const [password,setPassword]=useState('')
	const [isActive,setIsActive]=useState(false)

	// console.log(email)
	// console.log(password1)
	// console.log(password2)

	function loginUser(e){
		e.preventDefault()

		/*
			syntax:
				fetch('url',{options})
				.then(res=>res.json())
				.then(data=>{})
		*/
		fetch(`${process.env.REACT_APP_API_URL}/users/login`,{
			method:'POST',
			headers:{
				'Content-type':'application/json'
			},
			body:JSON.stringify({
				email:email,
				password:password
			})
		})
		.then(res=>res.json())
		.then(data=>{
			// console.log(data)
			if (typeof data.access !== "undefined"){
				localStorage.setItem('token',data.access)
				retrieveUserDetails(data.access)

				Swal.fire({
					title:'Login Successful',
					icon:'success',
					text:'Welcome to Zuitt'
				})
			} else {
				Swal.fire({
					title:'Authentication Failed',
					icon:'error',
					text:'Check your login details and try again'
				})
			}
		})

		// set the email from the authenticated user(logged in user) in the local storage
		/*
			syntax:
			localStorage.setItem('propertyName', value)
		*/
		// localStorage.setItem('email',email)

		// setUser({
		// 	email:localStorage.getItem('email')
		// })
		
		setEmail('')
		setPassword('')
		// alert('You are now logged in!')
	}

	const retrieveUserDetails=(token)=>{
		fetch('http://localhost:4000/users/details',{
			method:'GET',
			headers:{
				Authorization:`Bearer ${token}`
			}
		})
		.then(res=>res.json())
		.then(data=>{
			console.log(data)
			setUser({
				id:data._id,
				isAdmin:data.isAdmin
			})
		})
	}

	useEffect(()=>{
		if (email!=='' && password!=='') {
			setIsActive(true)
		} else {
			setIsActive(false)
		}
	},[email,password])

	return (
		(user.id!==null)? 
			<Navigate to ="/courses"/>
		:
		<Form onSubmit={e=>loginUser(e)}>
		<h2>Login</h2>
		  <Form.Group className="mb-3" controlId="userEmail">
		    <Form.Label>Email address</Form.Label>
		    <Form.Control 
		    				type="email" 
		    				placeholder="Enter email"
		    				value={email}
		    				onChange= {e=>setEmail(e.target.value)}
		    				required />
		    <Form.Text className="text-muted">
		      We'll never share your email with anyone else.
		    </Form.Text>
		  </Form.Group>

		  <Form.Group className="mb-3" controlId="password1">
		    <Form.Label>Password</Form.Label>
		    <Form.Control 
		    				type="password" 
		    				placeholder="Password"
		    				value={password}
		    				onChange= {e=>setPassword(e.target.value)}
		    				required />
		  </Form.Group>
		
		  {
		  	isActive ?
		  	<Button variant="success" type="submit" id="submitBtn">
		  	  Login
		  	</Button>
		  	:
		  	<Button variant="secondary" type="submit" id="submitBtn" disabled>
		  	  Login
		  	</Button>
		  }

		</Form>
	)
}