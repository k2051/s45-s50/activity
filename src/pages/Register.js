import {Form,Button} from 'react-bootstrap'
import {useState,useEffect,useContext} from 'react'
import UserContext from '../UserContext'
import {Navigate} from 'react-router-dom'
import Swal from 'sweetalert2'
export default function Register(){

	const [firstName,setFirstName]=useState('')
	const [lastName,setLastName]=useState('')
	const [mobileNo,setMobileNo]=useState('')
	
	// const [isDuplicate,setIsDuplicate]=useState(false)

	const [email,setEmail]=useState('')
	const [password1,setPassword1]=useState('')
	const [password2,setPassword2]=useState('')
	const [isActive,setIsActive]=useState(false)
	
	// const [isRegistered,setIsRegistered]=useState(false)

	const {user}=useContext(UserContext)

	// console.log(email)
	// console.log(password1)
	// console.log(password2)

	

	function registerUser(e){
		e.preventDefault()
		setEmail('')
		setPassword1('')
		setPassword2('')

		// fetch checkEmail route from backend API to check if email input exists already
		fetch('http://localhost:4000/users/checkEmail',{
			method:'POST',
			headers:{
				'Content-type':'application/json'
			},
			body:JSON.stringify({
				email:email
			})
		})
		.then(res=>res.json())
		.then(data=>{
			// result from the API route is true/false
			// will output true if email exists, false if not
			if (data){
				Swal.fire({
						title:'Email already exists',
						icon:'error',
						text:'Please provide a different email'
					})
			} else {
					fetch('http://localhost:4000/users/register',{
						method:'POST',
						headers:{
							'Content-type':'application/json'
						},
						body:JSON.stringify({
							firstName:firstName,
							lastName:lastName,
							mobileNo:mobileNo,
							email:email,
							password:password1				
						})
					})
					.then(res=>res.json())
					.then(data=>{
						if (data){
							Swal.fire({
								title:'Registration Successful!',
								icon:'success',
								text:'You may now login'
							})
						.then(function(){
							// once "ok" button is clicked, redirect to login page
							window.location = '/login'
						})
					} else {
						Swal.fire({
								title:'Registration Failed',
								icon:'error',
								text:'Please try again'
						})
					}
				})	
			}
		})
		
	}
	
	useEffect(()=>{
		if ((email!=='' && password1!=='' && password2!=='' && firstName!=='' && lastName!=='' && mobileNo!=='' && mobileNo.length===11) && (password1===password2))  {
			setIsActive(true)
		} else {
			setIsActive(false)
		}

		


	},[email,password1,password2,mobileNo,firstName,lastName])

	
		return (
			(user.id!==null)?
			<Navigate to='/courses' />
			:
			<Form onSubmit={e=>registerUser(e)}>
				<Form.Group className="mb-3" controlId="firstName">
				   <Form.Label>First Name</Form.Label>
				    <Form.Control 
				    				type="text" 
				    				placeholder="Enter First Name"
				    				value={firstName}
				    				onChange= {e=>setFirstName(e.target.value)}
				    				required />
				</Form.Group>

				<Form.Group className="mb-3" controlId="lastName">
				   <Form.Label>Last Name</Form.Label>
				    <Form.Control 
				    				type="text" 
				    				placeholder="Enter Last Name"
				    				value={lastName}
				    				onChange= {e=>setLastName(e.target.value)}
				    				required />
				</Form.Group>

				<Form.Group className="mb-3" controlId="mobileNo">
				   <Form.Label>Mobile Number</Form.Label>
				    <Form.Control 
				    				type="text"
				    				placeholder="Enter Mobile Number"
				    				value={mobileNo}
				    				onChange= {e=>setMobileNo(e.target.value)}
				    				required />
				</Form.Group>

				<Form.Group className="mb-3" controlId="userEmail">
				   <Form.Label>Email address</Form.Label>
				    <Form.Control 
				    				type="email" 
				    				placeholder="Enter email"
				    				value={email}
				    				onChange= {e=>setEmail(e.target.value)}
				    				required />
				    <Form.Text className="text-muted">
				      We'll never share your email with anyone else.
				    </Form.Text>
				</Form.Group>

				<Form.Group className="mb-3" controlId="password1">
				    <Form.Label>Password</Form.Label>
				    <Form.Control 
				    				type="password" 
				    				placeholder="Password"
				    				value={password1}
				    				onChange= {e=>setPassword1(e.target.value)}
				    				required />
				  </Form.Group>

				  <Form.Group className="mb-3" controlId="password2">
				    <Form.Label>Verify Password</Form.Label>
				    <Form.Control 
				    				type="password" 
				    				placeholder="Verify Password"
				    				value={password2}
				    				onChange= {e=>setPassword2(e.target.value)}
				    				required />
				  </Form.Group>
				
				  {
				  	isActive ?
				  	<Button variant="primary" type="submit" id="submitBtn">
				  	  Submit
				  	</Button>
				  	:
				  	<Button variant="danger" type="submit" id="submitBtn" disabled>
				  	  Submit
				  	</Button>
				  }

			</Form>
		)
}
